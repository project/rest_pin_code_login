<?php

namespace Drupal\rest_pin_code_login\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Rest pin code login settings for this site.
 */
class UserPinCodeForm extends FormBase {

  /**
   * The Pin code login helper service.
   *
   * @var \Drupal\rest_pin_code_login\RestPinCodeLoginHelper
   */
  protected $restPinCodeLoginHelper;

  /**
   * Constructs a new UserPinCodeForm.
   *
   * @param string $rest_pin_code_login_helper
   *   The Pin code login helper service.
   */
  public function __construct($rest_pin_code_login_helper) {
    $this->restPinCodeLoginHelper = $rest_pin_code_login_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('rest_pin_code_login.helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'rest_pin_code_login_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, UserInterface $user = NULL) {
    // Extract the user's key.
    $pin_code = $user->pin_code->value;

    // Store the user ID.
    $form['#uid'] = $user->id();

    $form['pin_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Pin code'),
      '#default_value' => $pin_code ?: '',
    ];

    $form['actions'] = [
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Save'),
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save pin code.
    User::load($form['#uid'])
      ->set('pin_code', $form_state->getValue('pin_code'))
      ->save();
  }

}

<?php

namespace Drupal\rest_pin_code_login\Controller;

use Drupal\Core\Access\CsrfTokenGenerator;
use Drupal\user\UserFloodControlInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\user\Controller\UserAuthenticationController;
use Drupal\user\UserAuthInterface;
use Drupal\user\UserStorageInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Serializer\Serializer;

/**
 * Returns responses for Rest pin code login routes.
 */
class RestPinCodeLoginController extends UserAuthenticationController {

  /** @var \Drupal\rest_pin_code_login\RestPinCodeLoginHelper */
  protected $pinCodeLoginHelper;

  public function __construct(UserFloodControlInterface $userFloodControl, UserStorageInterface $user_storage, CsrfTokenGenerator $csrf_token, UserAuthInterface $user_auth, RouteProviderInterface $route_provider, Serializer $serializer, array $serializer_formats, LoggerInterface $logger) {
    parent::__construct($userFloodControl, $user_storage, $csrf_token, $user_auth, $route_provider, $serializer, $serializer_formats, $logger);
    $this->pinCodeLoginHelper = \Drupal::service('rest_pin_code_login.helper');
  }

  /**
   * Logs in a user.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   A response which contains the ID and CSRF token.
   */
  public function login(Request $request) {
    $format = $this->getRequestFormat($request);

    $content = $request->getContent();
    $credentials = $this->serializer->decode($content, $format);
    if (!isset($credentials['pin_code'])) {
      throw new BadRequestHttpException('Missing credentials.pin_code.');
    }

    /** @var \Drupal\user\UserInterface $user */
    $user = $this->pinCodeLoginHelper->getUserByPinCode($credentials['pin_code']);
    if (empty($user)) {
      throw new BadRequestHttpException('Invalid pin code.');
    }

    $credentials['name'] = $user->getAccountName();

    $this->floodControl($request, $credentials['name']);

    if ($this->userIsBlocked($credentials['name'])) {
      throw new BadRequestHttpException('The user has not been activated or is blocked.');
    }

    if ($user) {
      $this->userFloodControl->clear('rest_pin_code_login.login', $this->getLoginFloodIdentifier($request, $credentials['name']));
      $this->userLoginFinalize($user);

      // Send basic metadata about the logged in user.
      $response_data = [];
      if ($user->get('uid')->access('view', $user)) {
        $response_data['current_user']['uid'] = $user->id();
      }
      if ($user->get('roles')->access('view', $user)) {
        $response_data['current_user']['roles'] = $user->getRoles();
      }
      if ($user->get('name')->access('view', $user)) {
        $response_data['current_user']['name'] = $user->getAccountName();
      }
      $response_data['csrf_token'] = $this->csrfToken->get('rest');

      $logout_route = $this->routeProvider->getRouteByName('user.logout.http');
      // Trim '/' off path to match \Drupal\Core\Access\CsrfAccessCheck.
      $logout_path = ltrim($logout_route->getPath(), '/');
      $response_data['logout_token'] = $this->csrfToken->get($logout_path);

      $encoded_response_data = $this->serializer->encode($response_data, $format);
      return new Response($encoded_response_data);
    }

    $flood_config = $this->config('user.flood');
    if ($identifier = $this->getLoginFloodIdentifier($request, $credentials['name'])) {
      $this->userFloodControl->register('rest_pin_code_login.login', $flood_config->get('user_window'), $identifier);
    }
    // Always register an IP-based failed login event.
    $this->userFloodControl->register('user.failed_login_ip', $flood_config->get('ip_window'));
    throw new BadRequestHttpException('Invalid pin code.');
  }

}

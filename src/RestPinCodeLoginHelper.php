<?php

namespace Drupal\rest_pin_code_login;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * RestPinCodeLoginHelper service.
 */
class RestPinCodeLoginHelper {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a RestPinCodeLoginHelper object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Load the user associated with a given key.
   *
   * @param $pin_code
   * @return \Drupal\Core\Entity\EntityInterface|\Drupal\user\Entity\User|null
   *   The matching user entity, or NULL if there was no match.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getUserByPinCode($pin_code) {
    // Load user storage.
    $storage = $this->entityTypeManager
      ->getStorage('user');

    // Query to find a user with this key.
    $user = $storage
      ->getQuery()
      ->condition('pin_code', $pin_code)
      ->execute();

    // Check if a user was found.
    if ($user) {
      // Load and return the user.
      return $storage->load(reset($user));
    }

    return NULL;
  }

}
